# Bet Analyzer GUI

### Summary

This is a graphical user interface based on 4 main parameters: competition, league, team and odd in addition, data and analysis elements are included.

### Motivation

The purpose of this project to make it easier for the user to establish relationships between these parameters and analyze data.

### Built With

- Java SE 8
- Java Swing

### Demonstration

![](betanalyzer.gif)