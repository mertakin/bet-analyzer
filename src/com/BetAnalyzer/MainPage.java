package com.BetAnalyzer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.ImageIcon;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import javax.swing.JComboBox;

import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionEvent;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.UIManager;
import java.awt.ComponentOrientation;
import java.awt.Cursor;

public class MainPage {

	MenuPage menu;
	private JFrame frame;
	private JTextField txtFootballBetAnalyzer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage window = new MainPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @return
	 */

	public MainPage(){
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		//frame.setMinimumSize(new Dimension(1280, 720));
		//frame.setMaximumSize(new Dimension(1280, 720));
		//frame.setPreferredSize(new Dimension(1280, 720));
		//frame.setSize(new Dimension(1280, 720));
		//frame.getContentPane().setPreferredSize(new Dimension(1280, 720));
		//frame.getContentPane().setSize(new Dimension(1280, 720));
		//frame.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
		//frame.setBackground(UIManager.getColor("CheckBox.interiorBackground"));
		frame.setBounds(100, 100, 640,640 );
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
	

		txtFootballBetAnalyzer = new JTextField();
		txtFootballBetAnalyzer.setMaximumSize(new Dimension(1000, 1000));
		txtFootballBetAnalyzer.setSize(new Dimension(100, 100));
		txtFootballBetAnalyzer.setRequestFocusEnabled(false);
		txtFootballBetAnalyzer.setOpaque(false);
		txtFootballBetAnalyzer.setAlignmentX(Component.LEFT_ALIGNMENT);
		txtFootballBetAnalyzer.setBounds(103, 13, 434, 28);
		txtFootballBetAnalyzer.setFont(new Font("Arial Black", Font.PLAIN, 15));
		txtFootballBetAnalyzer.setHorizontalAlignment(SwingConstants.CENTER);
		txtFootballBetAnalyzer.setText("Football Bet Analyzer ");
		frame.getContentPane().add(txtFootballBetAnalyzer);
		txtFootballBetAnalyzer.setColumns(10);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("C:\\Users\\MerT\\Documents\\gui icon main.png"));
		label.setBounds(181, 78, 277, 220);
		frame.getContentPane().add(label);

		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menu.createAndShowGUI();
			}
		});
		btnConfirm.setBounds(194, 458, 253, 23);
		frame.getContentPane().add(btnConfirm);

		JButton btnNewButton = new JButton("Check for updates");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// display/center the jdialog when the button is pressed

				String[] buttons = { "Install", "Cancel" };
				int i = 0;
				int returnValue = JOptionPane.showOptionDialog(null, "Database updates are available", "Updates",
						JOptionPane.OK_CANCEL_OPTION, 0, null, buttons, buttons[i]);

				if (returnValue == JOptionPane.OK_OPTION) {
					JOptionPane.showMessageDialog(null, "Updates are installed");

				}

			}

		});

		btnNewButton.setBounds(194, 386, 253, 23);
		frame.getContentPane().add(btnNewButton);

		JComboBox comboBox = new JComboBox<Object>();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"    Select Database", "   1", "   2", "   3"}));
		comboBox.setBounds(194, 311, 253, 20);
		frame.getContentPane().add(comboBox);
		
		((JLabel)comboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

		String[] x = { "a" };

	}

	private void dispose() {
		// TODO Auto-generated method stub

	}
}