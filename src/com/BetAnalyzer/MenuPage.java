package com.BetAnalyzer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import java.awt.event.*;
import javax.swing.*;



public class MenuPage extends JPanel  implements ActionListener, Runnable{

    final static int extraWindowWidth = 100;

    JSplitPane jSplitPane, jSplitPane2,jSplitPane3,jSplitPane4,jSplitPane5,jSplitPane6;
    JPanel jPanel2a, jPanel2b, jPanel3,jPanel4, A3,H1,H2,H3,H4,panel_2,panel_3,panel_4,panel_5,panel_6,panel_7,panel_8,panel_9,panel_10,panel_11,panel_1_1;
    static Timer timer;
    static JProgressBar progressBar,progressBarFirst,progressBar_1,progressBar_2,progressBar_3,progressBar_4,progressBar_5,progressBar_6,progressBar_7;
    public static JFrame frame; 
    static MenuPage menu;
    JEditorPane A1;
    JLabel A2,goal;
    JTextField T1,T2,txtxBetStatistics,textField_18,textField_17,textField_16,textField_15,textField_14,textField_13,textField_12,txtOverunder,txtO,txtU,textField_10,txtFirstHalfStatistics,textField_7,textField_8,textField_5,textField_6,textField_4,textField_3;
    
    private static JTextField atxtxBetStatistics;
	private static JTextField atxtOverunder;
	private static JTextField atxtFirstHalfStatistics;
	private static JTextField atextField_3;
	private static JTextField atextField_4;
	private static JTextField atextField_5;
	private static JTextField atextField_6;
	private static JTextField atextField_7;
	private static JTextField atextField_8;
	private static JTextField atxtU;
	private static JTextField atextField_10;
	private static JTextField atxtO;
	private static JTextField atextField_12;
	private static JTextField atextField_13;
	private static JTextField atextField_14;
	private static JTextField atextField_15;
	private static JTextField atextField_16;
	private static JTextField atextField_17;
	private static JTextField atextField_18;
	
	
   
   
    
    
    private static JOptionPane joption;
   

    
    
    static void CountDownProgressBar() {
        progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 10);
        progressBar.setValue(10);
        ActionListener listener = new ActionListener() {
            int counter = 10;
            public void actionPerformed(ActionEvent ae) {
                counter--;
                progressBar.setValue(counter);
                if (counter<1) {
                    
                	timer.stop();
                	
                	menu.analyzescreen();
                  

                	   
                } 
                
            }
        };
        timer = new Timer(1000, listener);
        timer.start();
        joption.showMessageDialog(null, progressBar);
       
    }
    
    

    

    

    public MenuPage(){
    }
    private final JList<String> list6=new JList<>();

    private final JList<String> list5=new JList<>(new String[]{ "3.20 3.20 1.60","1.10 4.50 8.0","2.25 4.50 1.90","1.60 3.00 4.00","6.50,4.00,1.25","2.10 2.90 2.60","2.80 3.00 1.95","1.40,3.5,5.0"});
    private final JList<String> list1=new JList<>(new String[]{"Turkish Super League", "Premier League","Spanish La Liga","Bundesliga"});
    private final JList<String> list2 = new JList<>();
    private final JList<String> list3=new JList<>(new String [] {"Turkish Super League", "Premier League","Spanish La Liga","Bundesliga"});
    private final List<DefaultListModel> models = new ArrayList<>();
    private final List<DefaultListModel> rmodels = new ArrayList<>();

    static JButton btnAnalyze = new JButton("Analyze");
    
    
    
    

    
    
    
    private final JComboBox combo1= new JComboBox(
    	new String[]{"Turkish Super League", "Premier League","Spanish La Liga","Bundesliga" }
    );
    private final JComboBox combo2=new JComboBox();
    private ComboBoxModel [] cmodels = new ComboBoxModel[4];
    
    private final JComboBox combo3=new JComboBox(
    		new String [] {"Select The Ratio"});
    




    public void addComponentToPane(Container pane) {
        JTabbedPane tabbedPane = new JTabbedPane();
        
        
        // Create the "cards".
        JPanel card1 = new JPanel() {
            // Make the panel wider than it really needs, so
            // the window's wide enough for the tabs to stay
            // in one row.
            public Dimension getPreferredSize() {
                Dimension size = super.getPreferredSize();
                size.width += extraWindowWidth;
                return size; 
            }
        };

        
        cmodels[0] = new DefaultComboBoxModel(
                new String[]{"Adanaspor", "Antalyaspor"});
        cmodels[1] = new DefaultComboBoxModel(
                new String[]{"Manchester United", "B2", "B3", "B4"});
        cmodels[2] = new DefaultComboBoxModel(
                new String[]{"Real Madrid", "C2"});
        
        cmodels[3] = new DefaultComboBoxModel(
                new String[]{"Bayern Munich", "C2"});

        combo2.setModel(cmodels[0]);
        this.add(combo1);
        this.add(combo2);
        this.add(combo3);
        this.add(btnAnalyze);
        combo1.addActionListener(this);
        
		btnAnalyze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// display/center the jdialog when the button is pressed
		        
		        	MenuPage.CountDownProgressBar();
		        	

			}

		});
        
        DefaultListModel<String> rmodel1 = new DefaultListModel<>();
        rmodel1.addElement("1 :%48");
        rmodel1.addElement("X :%26");
        rmodel1.addElement("2 :%26");
        rmodel1.addElement("1/1 :%32");
        rmodel1.addElement("1/0 :%14");
        rmodel1.addElement("1/2 :%5");
        rmodel1.addElement("X/1 :%36");
        rmodel1.addElement("X/X :%28");
        rmodel1.addElement("X/2 :%17");
        rmodel1.addElement("2/1 :%8");
        rmodel1.addElement("2/0 :%23");
        rmodel1.addElement("2/2 :%11");

        
        rmodels.add(rmodel1);
        
        DefaultListModel<String> rmodel2 = new DefaultListModel<>();
        rmodel2.addElement("1 :%82");
        rmodel2.addElement("X :%10");
        rmodel2.addElement("2 :%8");
        rmodel2.addElement("1/1 :%65");
        rmodel2.addElement("1/0 :%24");
        rmodel2.addElement("1/2 :%1");
        rmodel2.addElement("X/1 :%42");
        rmodel2.addElement("X/X :%11");
        rmodel2.addElement("X/2 :%6");
        rmodel2.addElement("2/1 :%14");
        rmodel2.addElement("2/0 :%7");
        rmodel2.addElement("2/2 :%2");

        rmodels.add(rmodel2);

        
        DefaultListModel<String> rmodel3 = new DefaultListModel<>();
        rmodel3.addElement("1 :%38");
        rmodel3.addElement("X :%36");
        rmodel3.addElement("2 :%36");
        rmodel3.addElement("1/1 :%22");
        rmodel3.addElement("1/0 :%24");
        rmodel3.addElement("1/2 :%5");
        rmodel3.addElement("X/1 :%36");
        rmodel3.addElement("X/X :%28");
        rmodel3.addElement("X/2 :%27");
        rmodel3.addElement("2/1 :%13");
        rmodel3.addElement("2/0 :%28");
        rmodel3.addElement("2/2 :%11");
        rmodels.add(rmodel3);

        
        
        
        
        

		
		
		
		
		
        
        
        
         DefaultListModel<String> model1 = new DefaultListModel<>();
         model1.addElement("Adanaspor");
         model1.addElement("Antalyaspor");
         model1.addElement("Gen�lerbirligi");
         model1.addElement("Osmanlispor FK");
         model1.addElement("Galatasaray SK");
         model1.addElement("Fenerbah�e SK");
         model1.addElement("Besikta� JK");
         model1.addElement("Trabzonspor");
         model1.addElement("Bursaspor");
         model1.addElement("Kasimpa�a");
         model1.addElement("Gaziantepspor");
         model1.addElement("Kayserispor");
         model1.addElement("�aykurrizespor");
         model1.addElement("Akhisar Belediye");
         model1.addElement("Alanyaspor");
         model1.addElement("Konyaspor");
         model1.addElement("Basaksehir");
         model1.addElement("Karab�kspor");
         models.add(model1);
            
            
            DefaultListModel<String> model2 = new DefaultListModel<>();
            model2.addElement("Manchester United");
            model2.addElement("Manchester City");
            model2.addElement("Liverpool FC");
            model2.addElement("Chelsea FC");
            model2.addElement("Arsenal FC");
            model2.addElement("Totenham Hotspur");
            model2.addElement("Everton FC");
            model2.addElement("Watford FC");
            model2.addElement("Burnley FC");
            model2.addElement("Southampton FC");
            model2.addElement("West Bromwich Albion");
            model2.addElement("Stoke City");
            model2.addElement("AFC Bournemouth");
            model2.addElement("Leicester City");
            model2.addElement("Middlesborugh FC");
            model2.addElement("Crystal Palace");
            model2.addElement("West Ham United");
            model2.addElement("Hull City");
            model2.addElement("Swansea City");
            models.add(model2);
            
            
            
            
            DefaultListModel<String> model3 = new DefaultListModel<>();
            model3.addElement("Real Madrid");
            model3.addElement("FC Barcelona");
            model3.addElement("Villarreal CF");
            model3.addElement("Atletico Madrid");
            model3.addElement("Real Sociedad");
            model3.addElement("Athletic Bilbao");
            model3.addElement("Celta de Vigo");
            model3.addElement("UD Las Palmas");
            model3.addElement("SD Eibar");
            model3.addElement("Deportivo Alaves");
            model3.addElement("RCD Espanyol");
            model3.addElement("Real Betis");
            model3.addElement("Valencia CF");
            model3.addElement("CD Leganes");
            model3.addElement("Deportivo de La Coruna");
            model3.addElement("Sporting Gijon");
            model3.addElement("CA Osasuna");
            model3.addElement("Granada CF");
            models.add(model3);
            
            
            
            
            DefaultListModel<String> model4 = new DefaultListModel<>();
            model4.addElement("Bayern Munich");
            model4.addElement("RasenBallsport Leipzig");
            model4.addElement("TSG 1899 Hoffenheim");
            model4.addElement("Hertha BSC");
            model4.addElement("Borussia Dortmund");
            model4.addElement("1.FC K�ln");
            model4.addElement("Eintracht Frankfurt");
            model4.addElement("Bayer 04 Leverkusen");
            model4.addElement("SC Freiburg");
            model4.addElement("1.FSV Mainz 05");
            model4.addElement("Borussia M�nchengladbach");
            model4.addElement("FC Schalke 04");
            model4.addElement("FC Augsburg");
            model4.addElement("VfL Wolfsburg");
            model4.addElement("SV Darmstadt 98");
            model4.addElement("SV Werder Bremen");
            model4.addElement("FC Ingolstadt 04");
            model4.addElement("Hamburger SV");
            models.add(model4);
            
            
            
            
            
            
            
            list2.setModel(model1);
            list1.addListSelectionListener((ListSelectionEvent e) -> {
                if (!e.getValueIsAdjusting()) {
                    list2.setModel(models.get(list1.getSelectedIndex()));
                }
            });

            	
            
            list6.setModel(rmodel1);
            list5.addListSelectionListener((ListSelectionEvent e) -> {
                if (!e.getValueIsAdjusting()) {
                    list6.setModel(rmodels.get(list5.getSelectedIndex()));
                }
            });
            

            
            

        jPanel2a = new JPanel(new BorderLayout());
        jPanel2b = new JPanel(new BorderLayout());
        jPanel3 = new JPanel(new BorderLayout());
        jPanel4 = new JPanel();
        GridBagLayout gbl_panel=new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		jPanel4.setLayout(gbl_panel);
    
     
        
        //about
        
        JEditorPane A1=new JEditorPane();
		A1.setFont(new Font("Arial Black", Font.PLAIN, 25));
		A1.setOpaque(false);
		A1.setText("Football Bet Analyzer has been updated to version 0.0.0.0.0 \r\nBibliography : www.bahiscipro.com www.transfermarkt.com\r\nContact us :  info@footballbetanalyzer.com\r\n www.footballbetanalyzer.com\r\nDeveloped by Sergen Boga & Mert Akin");
		A1.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		//card8.add(dtrpnAaaa, BorderLayout.CENTER);
		
		JLabel A2 = new JLabel("");
		A2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		A2.setIcon(new ImageIcon("C:\\Users\\MerT\\Documents\\gui.png"));
		//card8.add(A2, BorderLayout.SOUTH);
		
		JPanel A3 = new JPanel();
		//card8.add(A3, BorderLayout.NORTH);
		
		//help
        
		JTextField T1=new JTextField();
		T1.setSelectedTextColor(UIManager.getColor("Table.light"));
		T1.setOpaque(false);
		T1.setFont(new Font("Arial Black", Font.BOLD, 16));
		T1.setHorizontalAlignment(SwingConstants.CENTER);
		T1.setText("How can we help you?");
		//panel.add(T1, BorderLayout.NORTH);
		T1.setColumns(10);
		
		JPanel H1 = new JPanel();
		H1.setForeground(UIManager.getColor("Table.selectionBackground"));
		H1.setBackground(UIManager.getColor("Table.selectionBackground"));
		//panel.add(H1, BorderLayout.CENTER);
		H1.setLayout(new BorderLayout(0, 0));
		
		JPanel H2 = new JPanel();
		H1.add(H2, BorderLayout.WEST);
		
		JPanel H3 = new JPanel();
		H1.add(H3, BorderLayout.EAST);
		
		JPanel H4 = new JPanel();
		H1.add(H4, BorderLayout.SOUTH);
		
		T2 = new JTextField();
		T2.setFont(new Font("Arial Black", Font.BOLD, 18));
		T2.setHorizontalAlignment(SwingConstants.CENTER);
		T2.setText("If you have any problem,please contact us.");
		H1.add(T2, BorderLayout.CENTER);
		T2.setColumns(10);
		
        
        

        //League
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		jPanel4.add(panel_2, gbc_panel_2);
		
		txtxBetStatistics = new JTextField();
		txtxBetStatistics.setHorizontalAlignment(SwingConstants.CENTER);
		txtxBetStatistics.setFont(new Font("Arial", Font.BOLD, 11));
		txtxBetStatistics.setText(" 1X2 Bet statistics");
		panel_2.add(txtxBetStatistics);
		txtxBetStatistics.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 1;
		jPanel4.add(panel_3, gbc_panel_3);
		
		textField_17 = new JTextField();
		textField_17.setHorizontalAlignment(SwingConstants.CENTER);
		textField_17.setText("1");
		textField_17.setFont(new Font("Arial", Font.BOLD, 11));
		panel_3.add(textField_17);
		textField_17.setColumns(10);
		
		JProgressBar progressBarFirst = new JProgressBar();
		progressBarFirst.setValue(40);
		panel_3.add(progressBarFirst);
		
		textField_18 = new JTextField();
		textField_18.setText("%40");
		textField_18.setFont(new Font("Arial", Font.BOLD, 11));
		textField_18.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(textField_18);
		textField_18.setColumns(10);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 2;
		jPanel4.add(panel_4, gbc_panel_4);
		
		textField_15 = new JTextField();
		textField_15.setHorizontalAlignment(SwingConstants.CENTER);
		textField_15.setText("0");
		textField_15.setFont(new Font("Arial", Font.BOLD, 11));
		panel_4.add(textField_15);
		textField_15.setColumns(10);
		
		JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setValue(21);
		panel_4.add(progressBar_1);
		
		textField_16 = new JTextField();
		textField_16.setFont(new Font("Arial", Font.BOLD, 11));
		textField_16.setHorizontalAlignment(SwingConstants.CENTER);
		textField_16.setText("%21");
		panel_4.add(textField_16);
		textField_16.setColumns(10);
		
		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.insets = new Insets(0, 0, 5, 0);
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 3;
		jPanel4.add(panel_5, gbc_panel_5);
		
		textField_13 = new JTextField();
		textField_13.setText("2");
		textField_13.setHorizontalAlignment(SwingConstants.CENTER);
		textField_13.setFont(new Font("Arial", Font.BOLD, 11));
		panel_5.add(textField_13);
		textField_13.setColumns(10);
		
		JProgressBar progressBar_2 = new JProgressBar();
		progressBar_2.setValue(39);
		panel_5.add(progressBar_2);
		
		textField_14 = new JTextField();
		textField_14.setFont(new Font("Arial", Font.BOLD, 11));
		textField_14.setHorizontalAlignment(SwingConstants.CENTER);
		textField_14.setText("%39");
		panel_5.add(textField_14);
		textField_14.setColumns(10);
		
		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.insets = new Insets(0, 0, 5, 0);
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 4;
		jPanel4.add(panel_6, gbc_panel_6);
		
		txtOverunder = new JTextField();
		txtOverunder.setHorizontalAlignment(SwingConstants.CENTER);
		txtOverunder.setText("Over/Under 2,5");
		txtOverunder.setFont(new Font("Arial", Font.BOLD, 11));
		panel_6.add(txtOverunder);
		txtOverunder.setColumns(10);
		
		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.insets = new Insets(0, 0, 5, 0);
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 5;
		jPanel4.add(panel_7, gbc_panel_7);
		
		txtO = new JTextField();
		txtO.setHorizontalAlignment(SwingConstants.CENTER);
		txtO.setFont(new Font("Arial", Font.BOLD, 11));
		txtO.setText("O");
		panel_7.add(txtO);
		txtO.setColumns(10);
		
		JProgressBar progressBar_3 = new JProgressBar();
		progressBar_3.setValue(58);
		panel_7.add(progressBar_3);
		
		textField_12 = new JTextField();
		textField_12.setHorizontalAlignment(SwingConstants.CENTER);
		textField_12.setFont(new Font("Arial", Font.BOLD, 11));
		textField_12.setText("%58");
		panel_7.add(textField_12);
		textField_12.setColumns(10);
		
		JPanel panel_8 = new JPanel();
		GridBagConstraints gbc_panel_8 = new GridBagConstraints();
		gbc_panel_8.insets = new Insets(0, 0, 5, 0);
		gbc_panel_8.fill = GridBagConstraints.BOTH;
		gbc_panel_8.gridx = 0;
		gbc_panel_8.gridy = 6;
		jPanel4.add(panel_8, gbc_panel_8);
		
		txtU = new JTextField();
		txtU.setHorizontalAlignment(SwingConstants.CENTER);
		txtU.setFont(new Font("Arial", Font.BOLD, 11));
		txtU.setText("U");
		panel_8.add(txtU);
		txtU.setColumns(10);
		
		JProgressBar progressBar_4 = new JProgressBar();
		progressBar_4.setValue(42);
		panel_8.add(progressBar_4);
		
		textField_10 = new JTextField();
		textField_10.setHorizontalAlignment(SwingConstants.CENTER);
		textField_10.setFont(new Font("Arial", Font.BOLD, 11));
		textField_10.setText("%42");
		panel_8.add(textField_10);
		textField_10.setColumns(10);
		
		JPanel panel_9 = new JPanel();
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.insets = new Insets(0, 0, 5, 0);
		gbc_panel_9.fill = GridBagConstraints.BOTH;
		gbc_panel_9.gridx = 0;
		gbc_panel_9.gridy = 7;
		jPanel4.add(panel_9, gbc_panel_9);
		
		txtFirstHalfStatistics = new JTextField();
		txtFirstHalfStatistics.setHorizontalAlignment(SwingConstants.CENTER);
		txtFirstHalfStatistics.setFont(new Font("Arial", Font.BOLD, 11));
		txtFirstHalfStatistics.setText("First half statistics");
		panel_9.add(txtFirstHalfStatistics);
		txtFirstHalfStatistics.setColumns(10);
		
		JPanel panel_10 = new JPanel();
		GridBagConstraints gbc_panel_10 = new GridBagConstraints();
		gbc_panel_10.insets = new Insets(0, 0, 5, 0);
		gbc_panel_10.fill = GridBagConstraints.BOTH;
		gbc_panel_10.gridx = 0;
		gbc_panel_10.gridy = 8;
		jPanel4.add(panel_10, gbc_panel_10);
		
		textField_7 = new JTextField();
		textField_7.setHorizontalAlignment(SwingConstants.CENTER);
		textField_7.setFont(new Font("Arial", Font.BOLD, 11));
		textField_7.setText("1");
		panel_10.add(textField_7);
		textField_7.setColumns(10);
		
		JProgressBar progressBar_5 = new JProgressBar();
		progressBar_5.setValue(35);
		panel_10.add(progressBar_5);
		
		textField_8 = new JTextField();
		textField_8.setFont(new Font("Arial", Font.BOLD, 11));
		textField_8.setHorizontalAlignment(SwingConstants.CENTER);
		textField_8.setText("%35");
		panel_10.add(textField_8);
		textField_8.setColumns(10);
		
		JPanel panel_11 = new JPanel();
		GridBagConstraints gbc_panel_11 = new GridBagConstraints();
		gbc_panel_11.insets = new Insets(0, 0, 5, 0);
		gbc_panel_11.fill = GridBagConstraints.BOTH;
		gbc_panel_11.gridx = 0;
		gbc_panel_11.gridy = 9;
		jPanel4.add(panel_11, gbc_panel_11);
		
		textField_5 = new JTextField();
		textField_5.setText("0");
		textField_5.setFont(new Font("Arial", Font.BOLD, 11));
		textField_5.setHorizontalAlignment(SwingConstants.CENTER);
		panel_11.add(textField_5);
		textField_5.setColumns(10);
		
		JProgressBar progressBar_6 = new JProgressBar();
		progressBar_6.setValue(55);
		panel_11.add(progressBar_6);
		
		textField_6 = new JTextField();
		textField_6.setFont(new Font("Arial", Font.BOLD, 11));
		textField_6.setHorizontalAlignment(SwingConstants.CENTER);
		textField_6.setText("%55");
		panel_11.add(textField_6);
		textField_6.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 10;
		jPanel4.add(panel_1, gbc_panel_1);
		
		textField_4 = new JTextField();
		textField_4.setText("2");
		textField_4.setHorizontalAlignment(SwingConstants.CENTER);
		textField_4.setFont(new Font("Arial", Font.BOLD, 11));
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JProgressBar progressBar_7 = new JProgressBar();
		progressBar_7.setValue(10);
		panel_1.add(progressBar_7);
		
		textField_3 = new JTextField();
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setFont(new Font("Arial", Font.BOLD, 11));
		textField_3.setText("%10");
		panel_1.add(textField_3);
		textField_3.setColumns(10);		
		
		
		
		//main
		JLabel goal =new JLabel("");
		goal.setIcon(new ImageIcon("C:\\Users\\MerT\\Documents\\goal.jpg"));
		goal.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		
		
		
       





        jSplitPane2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, jPanel2a, jPanel2b);
        jSplitPane3 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, list1, list2);
        jSplitPane4 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,jSplitPane3,jPanel3);
        jSplitPane5 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,list3,jPanel4);
        jSplitPane6 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,list5,list6);


        



        
        JLabel lblHelloWorld = new JLabel("Hello World!");
        jPanel2a.add(lblHelloWorld);

        //jSplitPane2.setOneTouchExpandable(true);
        //jSplitPane2.setDividerLocation(100);


        //jSplitPane3.setOneTouchExpandable(true);
        //jSplitPane3.setDividerLocation(150);


        //jSplitPane4.setOneTouchExpandable(true);
        //jSplitPane4.setDividerLocation(300);



        JPanel card3 = new JPanel(new BorderLayout());
        JPanel card10=new JPanel(new BorderLayout());
    
        excelTojTable card5 = new excelTojTable();
        JPanel card6 = new JPanel(new BorderLayout());
        JPanel card7 = new JPanel(new BorderLayout());
        JPanel card8 = new JPanel(new BorderLayout());
        



        //card3.add(jSplitPane3,jSplitPane4);
        card3.add(jSplitPane4,BorderLayout.SOUTH);
        card3.add(jSplitPane5,BorderLayout.SOUTH);
        
        
        card8.add(A1, BorderLayout.CENTER);
        card8.add(A2, BorderLayout.SOUTH);
        card8.add(A3, BorderLayout.NORTH);
        
        card6.add(this);
        
        card7.add(T1, BorderLayout.NORTH);
        card7.add(H1, BorderLayout.CENTER);
        
        card10.add(goal,BorderLayout.CENTER);
        
        
        
        
        tabbedPane.addTab("Main",card10 );
        tabbedPane.addTab("Leagues",jSplitPane5);
        tabbedPane.addTab("Teams",jSplitPane4 );
        tabbedPane.addTab("Ratios", jSplitPane6);
        tabbedPane.addTab("Competitions", card5);
        tabbedPane.addTab("Analyze", card6);
        tabbedPane.addTab("Help", card7);
        tabbedPane.addTab("About", card8);

        pane.add(tabbedPane, BorderLayout.CENTER);
        
        


    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event dispatch thread.
     */
    
    public void actionPerformed(ActionEvent e) {
        int i = combo1.getSelectedIndex();
        combo2.setModel(cmodels[i]);
    }

    

    public static void createAndShowGUI() {
        // Create and set up the window.
        frame = new JFrame("BetAnalyzer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1920,1080);

        // Create and set up the content pane.
        MenuPage demo = new MenuPage();
        demo.addComponentToPane(frame.getContentPane());

        // Display the window.
        frame.pack();
        frame.setVisible(true);
        frame.setBounds(100, 100, 1280, 720);
		frame.setLocationRelativeTo(null);
        

    }
    
    
    public static void analyzescreen(){
    	frame=new JFrame("analyzeframe");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.pack();
    	frame.setVisible(true);
    	frame.setBounds(100, 100, 1920, 1080);
    	frame.setLocationRelativeTo(null);
    	
    	
    	JSplitPane AsplitPane=new JSplitPane();
    	frame.getContentPane().add(AsplitPane, BorderLayout.CENTER);
    	
    	JPanel Apanel=new JPanel();
    	AsplitPane.setLeftComponent(Apanel);
    	
    	JEditorPane Aeditor = new JEditorPane();
    	Aeditor.setOpaque(false);
    	Aeditor.setFont(new Font("Arial", Font.BOLD,11 ));
    	Aeditor.setText("Trust value over%71\r\nStandart devision %8\r\nComments......\r\n");
    	Apanel.add(Aeditor);
    	
    	JPanel A2panel=new JPanel();
    	AsplitPane.setRightComponent(A2panel);
    	
		GridBagLayout gbl_A2panel = new GridBagLayout();
		gbl_A2panel.columnWidths = new int[]{0, 0};
		gbl_A2panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_A2panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_A2panel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		A2panel.setLayout(gbl_A2panel);
		
		JPanel apanel_2 = new JPanel();
		GridBagConstraints gbc_apanel_2 = new GridBagConstraints();
		gbc_apanel_2.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_2.fill = GridBagConstraints.BOTH;
		gbc_apanel_2.gridx = 0;
		gbc_apanel_2.gridy = 0;
		A2panel.add(apanel_2, gbc_apanel_2);
		
		atxtxBetStatistics = new JTextField();
		atxtxBetStatistics.setHorizontalAlignment(SwingConstants.CENTER);
		atxtxBetStatistics.setFont(new Font("Arial", Font.BOLD, 11));
		atxtxBetStatistics.setText(" 1X2 Bet statistics");
		apanel_2.add(atxtxBetStatistics);
		atxtxBetStatistics.setColumns(10);
		
		JPanel apanel_3 = new JPanel();
		GridBagConstraints gbc_apanel_3 = new GridBagConstraints();
		gbc_apanel_3.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_3.fill = GridBagConstraints.BOTH;
		gbc_apanel_3.gridx = 0;
		gbc_apanel_3.gridy = 1;
		A2panel.add(apanel_3, gbc_apanel_3);
		
		atextField_17 = new JTextField();
		atextField_17.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_17.setText("1");
		atextField_17.setFont(new Font("Arial", Font.BOLD, 11));
		apanel_3.add(atextField_17);
		atextField_17.setColumns(10);
		
		JProgressBar aprogressBar = new JProgressBar();
		aprogressBar.setValue(40);
		apanel_3.add(aprogressBar);
		
		atextField_18 = new JTextField();
		atextField_18.setText("%40");
		atextField_18.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_18.setHorizontalAlignment(SwingConstants.CENTER);
		apanel_3.add(atextField_18);
		atextField_18.setColumns(10);
		
		JPanel apanel_4 = new JPanel();
		GridBagConstraints gbc_apanel_4 = new GridBagConstraints();
		gbc_apanel_4.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_4.fill = GridBagConstraints.BOTH;
		gbc_apanel_4.gridx = 0;
		gbc_apanel_4.gridy = 2;
		A2panel.add(apanel_4, gbc_apanel_4);
		
		atextField_15 = new JTextField();
		atextField_15.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_15.setText("0");
		atextField_15.setFont(new Font("Arial", Font.BOLD, 11));
		apanel_4.add(atextField_15);
		atextField_15.setColumns(10);
		
		JProgressBar aprogressBar_1 = new JProgressBar();
		aprogressBar_1.setValue(21);
		apanel_4.add(aprogressBar_1);
		
		atextField_16 = new JTextField();
		atextField_16.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_16.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_16.setText("%21");
		apanel_4.add(atextField_16);
		atextField_16.setColumns(10);
		
		JPanel apanel_5 = new JPanel();
		GridBagConstraints gbc_apanel_5 = new GridBagConstraints();
		gbc_apanel_5.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_5.fill = GridBagConstraints.BOTH;
		gbc_apanel_5.gridx = 0;
		gbc_apanel_5.gridy = 3;
		A2panel.add(apanel_5, gbc_apanel_5);
		
		atextField_13 = new JTextField();
		atextField_13.setText("2");
		atextField_13.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_13.setFont(new Font("Arial", Font.BOLD, 11));
		apanel_5.add(atextField_13);
		atextField_13.setColumns(10);
		
		JProgressBar aprogressBar_2 = new JProgressBar();
		aprogressBar_2.setValue(39);
		apanel_5.add(aprogressBar_2);
		
		atextField_14 = new JTextField();
		atextField_14.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_14.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_14.setText("%39");
		apanel_5.add(atextField_14);
		atextField_14.setColumns(10);
		
		JPanel apanel_6 = new JPanel();
		GridBagConstraints gbc_apanel_6 = new GridBagConstraints();
		gbc_apanel_6.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_6.fill = GridBagConstraints.BOTH;
		gbc_apanel_6.gridx = 0;
		gbc_apanel_6.gridy = 4;
		A2panel.add(apanel_6, gbc_apanel_6);
		
		atxtOverunder = new JTextField();
		atxtOverunder.setHorizontalAlignment(SwingConstants.CENTER);
		atxtOverunder.setText("Over/Under 2,5");
		atxtOverunder.setFont(new Font("Arial", Font.BOLD, 11));
		apanel_6.add(atxtOverunder);
		atxtOverunder.setColumns(10);
		
		JPanel apanel_7 = new JPanel();
		GridBagConstraints gbc_apanel_7 = new GridBagConstraints();
		gbc_apanel_7.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_7.fill = GridBagConstraints.BOTH;
		gbc_apanel_7.gridx = 0;
		gbc_apanel_7.gridy = 5;
		A2panel.add(apanel_7, gbc_apanel_7);
		
		atxtO = new JTextField();
		atxtO.setHorizontalAlignment(SwingConstants.CENTER);
		atxtO.setFont(new Font("Arial", Font.BOLD, 11));
		atxtO.setText("O");
		apanel_7.add(atxtO);
		atxtO.setColumns(10);
		
		JProgressBar aprogressBar_3 = new JProgressBar();
		aprogressBar_3.setValue(58);
		apanel_7.add(aprogressBar_3);
		
		atextField_12 = new JTextField();
		atextField_12.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_12.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_12.setText("%58");
		apanel_7.add(atextField_12);
		atextField_12.setColumns(10);
		
		JPanel apanel_8 = new JPanel();
		GridBagConstraints gbc_apanel_8 = new GridBagConstraints();
		gbc_apanel_8.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_8.fill = GridBagConstraints.BOTH;
		gbc_apanel_8.gridx = 0;
		gbc_apanel_8.gridy = 6;
		A2panel.add(apanel_8, gbc_apanel_8);
		
		atxtU = new JTextField();
		atxtU.setHorizontalAlignment(SwingConstants.CENTER);
		atxtU.setFont(new Font("Arial", Font.BOLD, 11));
		atxtU.setText("U");
		apanel_8.add(atxtU);
		atxtU.setColumns(10);
		
		JProgressBar aprogressBar_4 = new JProgressBar();
		aprogressBar_4.setValue(42);
		apanel_8.add(aprogressBar_4);
		
		atextField_10 = new JTextField();
		atextField_10.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_10.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_10.setText("%42");
		apanel_8.add(atextField_10);
		atextField_10.setColumns(10);
		
		JPanel apanel_9 = new JPanel();
		GridBagConstraints gbc_apanel_9 = new GridBagConstraints();
		gbc_apanel_9.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_9.fill = GridBagConstraints.BOTH;
		gbc_apanel_9.gridx = 0;
		gbc_apanel_9.gridy = 7;
		A2panel.add(apanel_9, gbc_apanel_9);
		
		atxtFirstHalfStatistics = new JTextField();
		atxtFirstHalfStatistics.setHorizontalAlignment(SwingConstants.CENTER);
		atxtFirstHalfStatistics.setFont(new Font("Arial", Font.BOLD, 11));
		atxtFirstHalfStatistics.setText("First half statistics");
		apanel_9.add(atxtFirstHalfStatistics);
		atxtFirstHalfStatistics.setColumns(10);
		
		JPanel apanel_10 = new JPanel();
		GridBagConstraints gbc_apanel_10 = new GridBagConstraints();
		gbc_apanel_10.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_10.fill = GridBagConstraints.BOTH;
		gbc_apanel_10.gridx = 0;
		gbc_apanel_10.gridy = 8;
		A2panel.add(apanel_10, gbc_apanel_10);
		
		atextField_7 = new JTextField();
		atextField_7.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_7.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_7.setText("1");
		apanel_10.add(atextField_7);
		atextField_7.setColumns(10);
		
		JProgressBar aprogressBar_5 = new JProgressBar();
		aprogressBar_5.setValue(35);
		apanel_10.add(aprogressBar_5);
		
		atextField_8 = new JTextField();
		atextField_8.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_8.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_8.setText("%35");
		apanel_10.add(atextField_8);
		atextField_8.setColumns(10);
		
		JPanel apanel_11 = new JPanel();
		GridBagConstraints gbc_apanel_11 = new GridBagConstraints();
		gbc_apanel_11.insets = new Insets(0, 0, 5, 0);
		gbc_apanel_11.fill = GridBagConstraints.BOTH;
		gbc_apanel_11.gridx = 0;
		gbc_apanel_11.gridy = 9;
		A2panel.add(apanel_11, gbc_apanel_11);
		
		atextField_5 = new JTextField();
		atextField_5.setText("0");
		atextField_5.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_5.setHorizontalAlignment(SwingConstants.CENTER);
		apanel_11.add(atextField_5);
		atextField_5.setColumns(10);
		
		JProgressBar aprogressBar_6 = new JProgressBar();
		aprogressBar_6.setValue(55);
		apanel_11.add(aprogressBar_6);
		
		atextField_6 = new JTextField();
		atextField_6.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_6.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_6.setText("%55");
		apanel_11.add(atextField_6);
		atextField_6.setColumns(10);
		
		JPanel apanel_1 = new JPanel();
		GridBagConstraints gbc_apanel_1 = new GridBagConstraints();
		gbc_apanel_1.fill = GridBagConstraints.BOTH;
		gbc_apanel_1.gridx = 0;
		gbc_apanel_1.gridy = 10;
		A2panel.add(apanel_1, gbc_apanel_1);
		
		atextField_4 = new JTextField();
		atextField_4.setText("2");
		atextField_4.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_4.setFont(new Font("Arial", Font.BOLD, 11));
		apanel_1.add(atextField_4);
		atextField_4.setColumns(10);
		
		JProgressBar aprogressBar_7 = new JProgressBar();
		aprogressBar_7.setValue(10);
		apanel_1.add(aprogressBar_7);
		
		atextField_3 = new JTextField();
		atextField_3.setHorizontalAlignment(SwingConstants.CENTER);
		atextField_3.setFont(new Font("Arial", Font.BOLD, 11));
		atextField_3.setText("%10");
		apanel_1.add(atextField_3);
		atextField_3.setColumns(10);
    	
    	
    }
    

    
    public static void main(String[] args) {
        EventQueue.invokeLater(new MenuPage());
    }
    
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}